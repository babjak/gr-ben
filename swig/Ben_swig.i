/* -*- c++ -*- */

#define BEN_API

%include "gnuradio.i"			// the common stuff

//load generated python docstrings
%include "Ben_swig_doc.i"

%{
#include "Ben/ieee802_15_4g_param_tagger_cc.h"
#include "Ben/symbol_carrier_mapper_cc.h"
#include "Ben/zero_padding_in_fr_cc.h"
%}


%include "Ben/ieee802_15_4g_param_tagger_cc.h"
GR_SWIG_BLOCK_MAGIC2(Ben, ieee802_15_4g_param_tagger_cc);

%include "Ben/symbol_carrier_mapper_cc.h"
GR_SWIG_BLOCK_MAGIC2(Ben, symbol_carrier_mapper_cc);
%include "Ben/zero_padding_in_fr_cc.h"
GR_SWIG_BLOCK_MAGIC2(Ben, zero_padding_in_fr_cc);
