/* -*- c++ -*- */
/* 
 * Copyright 2014 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */


#ifndef INCLUDED_BEN_IEEE802_15_4G_PARAM_TAGGER_CC_H
#define INCLUDED_BEN_IEEE802_15_4G_PARAM_TAGGER_CC_H

#include <Ben/api.h>
#include <gnuradio/sync_block.h>

namespace gr {
namespace Ben {

  /*!
   * \brief <+description of block+>
   * \ingroup Ben
   *
   */
  class BEN_API ieee802_15_4g_param_tagger_cc 
  : 
    virtual public gr::sync_block
  {
    public:
      typedef boost::shared_ptr<ieee802_15_4g_param_tagger_cc> sptr;

      /*!
       * \brief Return a shared_ptr to a new instance of Ben::ieee802_15_4g_param_tagger_cc.
       *
       * To avoid accidental use of raw pointers, Ben::ieee802_15_4g_param_tagger_cc's
       * constructor is in a private implementation
       * class. Ben::ieee802_15_4g_param_tagger_cc::make is the public interface for
       * creating new instances.
       */
      static sptr make(int option);
       
      virtual int     option() const = 0;
      virtual void    set_option(int option) = 0;
  };

} // namespace Ben
} // namespace gr

#endif /* INCLUDED_BEN_IEEE802_15_4G_PARAM_TAGGER_CC_H */

