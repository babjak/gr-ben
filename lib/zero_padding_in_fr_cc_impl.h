/* -*- c++ -*- */
/* 
 * Copyright 2014 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_BEN_ZERO_PADDING_IN_FR_CC_IMPL_H
#define INCLUDED_BEN_ZERO_PADDING_IN_FR_CC_IMPL_H

#include <Ben/zero_padding_in_fr_cc.h>

namespace gr {
namespace Ben {

class zero_padding_in_fr_cc_impl
: 
  public zero_padding_in_fr_cc
{
  private:
    int d_out_symbol_len;
    int d_in_symbol_len;
    unsigned short d_start_idx;
            
  public:
    zero_padding_in_fr_cc_impl(int out_symbol_len);
    ~zero_padding_in_fr_cc_impl();

    // Where all the action really happens
    void forecast (int noutput_items, gr_vector_int &ninput_items_required);

    int general_work(int                        noutput_items,
                    gr_vector_int               &ninput_items,
                    gr_vector_const_void_star   &input_items,
                    gr_vector_void_star         &output_items);
            
    ////////////////////////////////////
    int out_symbol_len() const { return d_out_symbol_len; }
    void set_out_symbol_len(int out_symbol_len) 
    { 
      d_out_symbol_len = out_symbol_len;
      d_start_idx = (d_out_symbol_len - d_in_symbol_len)/2;
                
      /////////////////////////
      // GNU radio HACK
      set_output_multiple(d_out_symbol_len);
    }
};

} // namespace Ben
} // namespace gr

#endif /* INCLUDED_BEN_ZERO_PADDING_IN_FR_CC_IMPL_H */
