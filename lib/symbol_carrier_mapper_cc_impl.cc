/* -*- c++ -*- */
/* 
 * Copyright 2014 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>

//#define DEBUGMODE 0

#if DEBUGMODE == 0
#define pdebug0(...) do{printf(__VA_ARGS__);}while(0)
#define pdebug1(...) do{}while(0)
#elif DEBUGMODE == 1
#define pdebug0(...) do{printf(__VA_ARGS__);}while(0)
#define pdebug1(...) do{printf(__VA_ARGS__);}while(0)
#else
#define pdebug0(...) do{}while(0)
#define pdebug1(...) do{}while(0)
#endif

#include <gnuradio/io_signature.h>
#include "symbol_carrier_mapper_cc_impl.h"
#include <math.h>
 
namespace gr {
namespace Ben {

///////////////////////////////////////////////////////////////////////////////////
//
//  Make.
//
///////////////////////////////////////////////////////////////////////////////////
symbol_carrier_mapper_cc::sptr
symbol_carrier_mapper_cc::make()
{
  return gnuradio::get_initial_sptr(new symbol_carrier_mapper_cc_impl());
}

    
///////////////////////////////////////////////////////////////////////////////////
//
//  The private constructor
//
///////////////////////////////////////////////////////////////////////////////////
symbol_carrier_mapper_cc_impl::symbol_carrier_mapper_cc_impl()
: 
  gr::block("symbol_carrier_mapper_cc",
              gr::io_signature::make(1, 1, sizeof(gr_complex)),
              gr::io_signature::make(1, 1, sizeof(gr_complex)))
{
    d_range[0] = 0;
    d_range[1] = 0;
    d_out_symbol_len = 1;

    d_unused_sets.clear();
    d_unused_set_cnt = 0;
    
    d_pilot_sets.clear();
    d_pilot_set_cnt = 0;
    
    d_pilot_gen = new additive_scrambler(0x1ff);
    d_pilot_gen->reset();
    
    set_tag_propagation_policy( TPP_DONT );
    
    /////////////////////////
    // GNU radio HACK
    d_hack_nout_size = 0;
}


///////////////////////////////////////////////////////////////////////////////////
//
// Our virtual destructor.
//
///////////////////////////////////////////////////////////////////////////////////
symbol_carrier_mapper_cc_impl::~symbol_carrier_mapper_cc_impl()
{
  delete d_pilot_gen;
}


///////////////////////////////////////////////////////////////////////////////////
//
// Forecasting output size.
//
///////////////////////////////////////////////////////////////////////////////////
void
symbol_carrier_mapper_cc_impl::forecast (int noutput_items, gr_vector_int &ninput_items_required)
{
  ninput_items_required[0] = noutput_items;
}


///////////////////////////////////////////////////////////////////////////////////
//
// 
//
///////////////////////////////////////////////////////////////////////////////////
#define NO_OUTPUT_SPACE_LEFT    0
#define NOT_ENOUGH_INPUT        1

unsigned char
symbol_carrier_mapper_cc_impl::Generate_OFDM_Symbols (const gr_complex* &in,
                                                    const int           ninput_items,
                                                    gr_complex*         &out,
                                                    const int           noutput_items,
                                                    int                 &nin,
                                                    int                 &nout)
{
  while (1)
  {
    pdebug1("Symbol mapper: nout: %d, d_out_symbol_len: %d, noutput_items: %d\n", nout, d_out_symbol_len, noutput_items);
    if ( nout + d_out_symbol_len > noutput_items )
      return NO_OUTPUT_SPACE_LEFT;
            
    //Reset output
    pdebug1("Symbol mapper: Reset output\n");
    memset( out + nout, 0, d_out_symbol_len * sizeof(gr_complex) );
        
    //Copy data and pilots
    std::vector<int16_t>::iterator unused_it;
        
    if ( d_unused_sets.size() )
      unused_it = d_unused_sets[d_unused_set_cnt].begin();
        
    std::vector<int16_t>::iterator pilot_it;
        
    if ( d_pilot_sets.size() )
      pilot_it = d_pilot_sets[d_pilot_set_cnt].begin();
        
        
    size_t in_symbol_len_so_far = 0;
    size_t out_symbol_len_so_far = 0;
    
    pdebug1("Symbol mapper: out_symbol_len_so_far: %lu, d_out_symbol_len: %d, nin: %d, in_symbol_len_so_far: %lu, ninput_items: %d\n", out_symbol_len_so_far, d_out_symbol_len, nin, in_symbol_len_so_far, ninput_items);
    for ( ; out_symbol_len_so_far < d_out_symbol_len && nin + in_symbol_len_so_far < ninput_items ; out_symbol_len_so_far++ )
    {
      int16_t carrier = (int64_t) out_symbol_len_so_far + d_range[0];
      pdebug1("Symbol mapper: carrier: %d\n", carrier);

      pdebug1("Symbol mapper: d_unused_sets.size(): %lu\n", d_unused_sets.size());
      if ( d_unused_sets.size() )
      {
        pdebug1("Symbol mapper: *unused_it: %d\n", *unused_it);
        while ( *unused_it < carrier && unused_it != d_unused_sets[d_unused_set_cnt].end() )
          unused_it++;
                    
        if ( unused_it != d_unused_sets[d_unused_set_cnt].end() && *unused_it == carrier )
        {
          *( out + nout + out_symbol_len_so_far ) = gr_complex(0.0, 0.0);
          pdebug1("Symbol mapper: Found an unused carrier: %d\n", carrier);
          continue;
        }
      }
          
      pdebug1("Symbol mapper: d_pilot_sets.size(): %lu\n", d_pilot_sets.size());
      if ( d_pilot_sets.size() )
      {
          pdebug1("Symbol mapper: *pilot_it: %d\n", *pilot_it);
          while ( *pilot_it < carrier && pilot_it != d_pilot_sets[d_pilot_set_cnt].end() )
            pilot_it++;
                  
          if ( pilot_it != d_pilot_sets[d_pilot_set_cnt].end() && *pilot_it == carrier )
          {
            *( out + nout + out_symbol_len_so_far ) = d_pilot_gen->next_bit() ? gr_complex(1.0, 0.0) : gr_complex(-1.0, 0.0);
            pdebug1("Symbol mapper: Found a pilot at: %d\n", carrier);
            continue;
          }
      }    
      
      *( out + nout + out_symbol_len_so_far ) = *( in + nin + in_symbol_len_so_far );
      in_symbol_len_so_far++;
      pdebug1("Symbol mapper: out_symbol_len_so_far: %lu, d_out_symbol_len: %d, nin: %d, in_symbol_len_so_far: %lu, ninput_items: %d\n", out_symbol_len_so_far, d_out_symbol_len, nin, in_symbol_len_so_far, ninput_items);
    }    
                
    if ( out_symbol_len_so_far < d_out_symbol_len )
      return NOT_ENOUGH_INPUT;
            
    //Change pilot and unused sets
    if ( d_unused_sets.size() )
      d_unused_set_cnt = (d_unused_set_cnt + 1) % d_unused_sets.size();
        
    if ( d_pilot_sets.size() )
    {
      d_pilot_set_cnt = (d_pilot_set_cnt + 1) % d_pilot_sets.size();

      if (d_pilot_set_cnt == 0)
        d_pilot_gen->reset();
    }
        
    nin  += in_symbol_len_so_far;
    nout += d_out_symbol_len;
  }
}


///////////////////////////////////////////////////////////////////////////////////
//
// 
//
///////////////////////////////////////////////////////////////////////////////////
void
symbol_carrier_mapper_cc_impl::Process_Stream_Tag(pmt::pmt_t &mapping)
{
#ifdef DEBUGMODE
  pmt::pmt_t key_list = pmt::dict_keys( mapping );
  pdebug0("Symbol mapper: mapping keys no: %lu\n", pmt::length(key_list));
  for (size_t i = 0; i < pmt::length(key_list); i++ )
    pdebug0("Symbol mapper: mapping keys %lu.: %s\n", i, pmt::symbol_to_string( pmt::nth( i, key_list ) ).c_str());
#endif
    
  pmt::pmt_t range = pmt::dict_ref( mapping, pmt::string_to_symbol("range"), pmt::PMT_NIL );
    
  pdebug0("Symbol mapper: range != pmt::PMT_NIL: %s\n", range != pmt::PMT_NIL ? "true" : "false");
  pdebug0("Symbol mapper: pmt::is_s16vector(range): %s\n", pmt::is_s16vector(range) ? "true" : "false");
  pdebug0("Symbol mapper: pmt::length(range) == 2: %s\n", pmt::length(range) == 2 ? "true" : "false");
  if ( range != pmt::PMT_NIL && pmt::is_s16vector(range) && pmt::length(range) == 2 )
  {
    d_range[0] = fmin( pmt::s16vector_elements( range )[0], pmt::s16vector_elements( range )[1] );
    d_range[1] = fmax( pmt::s16vector_elements( range )[0], pmt::s16vector_elements( range )[1] );

    pdebug0("Symbol mapper: d_range[1]: %d, d_range[0]: %d\n", d_range[1], d_range[0]);
    d_out_symbol_len = d_range[1] - d_range[0] + 1;
    pdebug0("Symbol mapper: d_out_symbol_len: %d\n", d_out_symbol_len);
    printf("Symbol mapper: Output range: %d - %d + 1 = %d\n", d_range[1], d_range[0], d_out_symbol_len);
  }

  pmt::pmt_t pilot_sets = pmt::dict_ref( mapping, pmt::string_to_symbol("pilot_sets"), pmt::PMT_NIL );
  if ( pilot_sets != pmt::PMT_NIL && pmt::is_vector(pilot_sets) )
  {
    d_pilot_sets.clear();
    d_pilot_set_cnt = 0;
    d_pilot_gen->reset();

    for ( size_t i = 0; i < pmt::length(pilot_sets); i++ )
    {
      pmt::pmt_t pilot_set = pmt::vector_ref(pilot_sets, i);

      if ( !pmt::is_s16vector(pilot_set) )
        continue;

      std::vector<int16_t> t( pmt::s16vector_elements( pilot_set ) );
      std::sort( t.begin(), t.end() );

      printf("Symbol mapper: Pilot set: ");
      for ( size_t i = 0; i < t.size(); i++) 
        printf("%d, ", t[i]);
      printf("\n");

      d_pilot_sets.push_back( t );  
    }
  }

  pmt::pmt_t unused_sets = pmt::dict_ref( mapping, pmt::string_to_symbol("unused_sets"), pmt::PMT_NIL );
  if ( unused_sets != pmt::PMT_NIL && pmt::is_vector(unused_sets) )
  {
    d_unused_sets.clear();

    for ( size_t i = 0; i < pmt::length(unused_sets); i++ )
    {
      pmt::pmt_t unused_set = pmt::vector_ref(unused_sets, i);

      if ( !pmt::is_s16vector(unused_set) )
        continue;

      std::vector<int16_t> t( pmt::s16vector_elements( unused_set ) );
      std::sort( t.begin(), t.end() );

      printf("Symbol mapper: Unused carrier set: ");
      for ( size_t i = 0; i < t.size(); i++) 
        printf("%d, ", t[i]);
      printf("\n");
            
      d_unused_sets.push_back( t );
    }
  }  
}


///////////////////////////////////////////////////////////////////////////////////
//
// Work method.
//
///////////////////////////////////////////////////////////////////////////////////
int
symbol_carrier_mapper_cc_impl::general_work (int                      noutput_items,
                                            gr_vector_int             &ninput_items,
                                            gr_vector_const_void_star &input_items,
                                            gr_vector_void_star       &output_items)
{
  pdebug1("Symbol mapper: d_hack_nout_size: %d\n", d_hack_nout_size);
  pdebug1("Symbol mapper: noutput_items: %d\n", noutput_items);
  
  /////////////////////////
  // GNU radio HACK
  if ( d_hack_nout_size > noutput_items )
  {
    // Tell runtime system how many input items we consumed on
    // each input stream.
    consume_each (0);

    // Tell runtime system how many output items we produced.
    return 0;
  }
  d_hack_nout_size = noutput_items;
  /////////////////////////
  
  
  const gr_complex*   in          = (const gr_complex*) input_items[0];
  gr_complex*         out         = (gr_complex*) output_items[0];
  int                 nin         = 0; //no. of processed input samples
  int                 nout        = 0; //no. of generated output samples

  uint64_t            abs_N       = nitems_read(0);
  uint64_t            end_abs_N   = abs_N + (uint64_t) ninput_items[0];
  
  std::vector<tag_t>           tags;
  get_tags_in_range( tags, 0, abs_N, end_abs_N );
  std::vector<tag_t>::iterator tags_itr;
  tags_itr = tags.begin();
  
  int in_stop_idx = nin; 
  unsigned char ret_val;
  
  do
  {
    //Look for the end of input buffer or the next tag with different setup
    pdebug1("Symbol mapper: Look for the end of input buffer or the next tag with different setup\n");
    nin = in_stop_idx; 
    in_stop_idx = ninput_items[0];

    for ( ; tags_itr != tags.end(); tags_itr++ )
    {
      pdebug0( "Symbol mapper: Found a stream tag: %s\n", pmt::symbol_to_string(tags_itr->key).c_str() );
      if ( pmt::symbol_to_string(tags_itr->key) != "symbol_mapping" || !pmt::is_dict(tags_itr->value) )
        continue;

      int tag_idx = tags_itr->offset - abs_N;
      pdebug0( "Symbol mapper: nin: %d, tag_idx: %d\n", nin, tag_idx );
      if ( nin > tag_idx )
        continue;

      if ( nin < tag_idx )
      {
        in_stop_idx = tag_idx;
        pdebug0( "Symbol mapper: in_stop_idx: %d\n", in_stop_idx );
        break;
      }

#ifdef DEBUGMODE
      pmt::pmt_t key_list = pmt::dict_keys( tags_itr->value );
      pdebug0("Symbol mapper: 1 mapping keys no: %lu\n", pmt::length(key_list));
      for (size_t i = 0; i < pmt::length(key_list); i++ )
        pdebug0("Symbol mapper: 1 mapping keys %lu.: %s\n", i, pmt::symbol_to_string( pmt::nth( i, key_list ) ).c_str());
#endif

      //There was a new stream tag with a different setup
      printf("Symbol mapper: New stream tag with a different setup detected at sample %lu.\n", tags_itr->offset );
      Process_Stream_Tag( tags_itr->value );
          
      pdebug0("Symbol mapper: Adding output tag at nitems_written(0) + nout: %lu\n", nitems_written(0) + nout);
      add_item_tag( 0, 
                    nitems_written(0) + nout, 
                    tags_itr->key, 
                    tags_itr->value,
                    pmt::mp(alias()) );
    }

    //Process input samples and generate OFDM symbols until 
    //  the end of input buffer or the next tag with different setup (in_stop_idx)
    //  OR
    //  we run out of output space 
    pdebug1("Symbol mapper: Process input samples and generate OFDM symbols\n");
    ret_val = Generate_OFDM_Symbols (in, in_stop_idx,
                                    out, noutput_items,
                                    nin,
                                    nout);
              
    pdebug1("Symbol mapper: ret_val: %s\n", ret_val == NO_OUTPUT_SPACE_LEFT ? "NO_OUTPUT_SPACE_LEFT" : "NOT_ENOUGH_INPUT");

  } while ( ret_val == NOT_ENOUGH_INPUT && tags_itr != tags.end() && ret_val != NO_OUTPUT_SPACE_LEFT );
 
  pdebug1("Symbol mapper: nin: %d\n", nin);
  pdebug1("Symbol mapper: nout: %d\n", nout);
  // Tell runtime system how many input items we consumed on
  // each input stream.
  consume_each (nin);

  // Tell runtime system how many output items we produced.
  return nout;
}


} /* namespace ieee802_15_4g */
} /* namespace gr */
