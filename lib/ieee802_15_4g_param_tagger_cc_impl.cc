/* -*- c++ -*- */
/* 
 * Copyright 2014 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

//#define DEBUGMODE 1
#ifdef DEBUGMODE
#include <stdio.h>
#define pdebug(...) do{if(DEBUGMODE)printf(__VA_ARGS__);}while(0)
#else
#define pdebug(...) do{}while(0)
#endif

#include <gnuradio/io_signature.h>
#include "ieee802_15_4g_param_tagger_cc_impl.h"

namespace gr {
namespace Ben {

///////////////////////////////////////////////////////////////////////////////////
//
//  Make.
//
///////////////////////////////////////////////////////////////////////////////////
ieee802_15_4g_param_tagger_cc::sptr
ieee802_15_4g_param_tagger_cc::make(int option)
{
  return gnuradio::get_initial_sptr(new ieee802_15_4g_param_tagger_cc_impl(option));
}

///////////////////////////////////////////////////////////////////////////////////
//
//  The private constructor
//
///////////////////////////////////////////////////////////////////////////////////
ieee802_15_4g_param_tagger_cc_impl::ieee802_15_4g_param_tagger_cc_impl(int option)
:
  gr::sync_block("ieee802_15_4g_param_tagger_cc",
                gr::io_signature::make(1, 1, sizeof(gr_complex)),
                gr::io_signature::make(1, 1, sizeof(gr_complex))),
  d_option(option),
  d_option_prev(0),
  d_residue(0),
  d_symbol_len(0)
{
    set_tag_propagation_policy( TPP_ALL_TO_ALL );
}

///////////////////////////////////////////////////////////////////////////////////
//
// Our virtual destructor.
//
///////////////////////////////////////////////////////////////////////////////////
ieee802_15_4g_param_tagger_cc_impl::~ieee802_15_4g_param_tagger_cc_impl()
{
}


///////////////////////////////////////////////////////////////////////////////////
//
// 
//
///////////////////////////////////////////////////////////////////////////////////
pmt::pmt_t
ieee802_15_4g_param_tagger_cc_impl::Generate_Mapping_Symbol()
{
  pmt::pmt_t mapping = pmt::make_dict();

  pdebug("Symbol tagger: d_option: %d\n", d_option);

  switch ( d_option )
  {
    case 1:
    {
      // Option 1
      //Range
      int16_t range_arr[2] = {-52, 52};
      pmt::pmt_t range        = pmt::init_s16vector(2, range_arr);

      //Unused carriers
      int16_t unused_set_arr[1] = {0};

      pmt::pmt_t unused_sets = pmt::make_vector(1, pmt::PMT_NIL);
      pmt::vector_set(unused_sets, 0, pmt::init_s16vector(1, unused_set_arr));

      //Pilot carriers
      int16_t pilot_sets_arr[13][8] = {
        {-38, -26, -14,  -2,  10, 22, 34, 46}, // 1
        {-46, -34, -22, -10,   2, 14, 26, 38}, // 2
        {-42, -30, -18,  -6,   6, 18, 30, 42}, // 3
        {-50, -38, -26, -14,  -2, 10, 22, 50}, // 4
        {-46, -34, -22, -10,   2, 14, 34, 46}, // 5
        {-42, -30, -18,  -6,   6, 18, 26, 38}, // 6
        {-50, -38, -26, -14,  -2, 30, 42, 50}, // 7
        {-46, -34, -22, -10,  10, 22, 34, 46}, // 8
        {-42, -30, -18,  -6,   2, 14, 26, 38}, // 9
        {-50, -38, -26,   6,  18, 30, 42, 50}, // 10
        {-46, -34, -14,  -2,  10, 22, 34, 46}, // 11
        {-42, -30, -22, -10,   2, 14, 26, 38}, // 12
        {-50, -18,  -6,   6,  18, 30, 42, 50}  // 13
      };
            
      pmt::pmt_t pilot_sets = pmt::make_vector(13, pmt::PMT_NIL);
      pmt::vector_set(pilot_sets,  0, pmt::init_s16vector(8, pilot_sets_arr[0]));
      pmt::vector_set(pilot_sets,  1, pmt::init_s16vector(8, pilot_sets_arr[1]));
      pmt::vector_set(pilot_sets,  2, pmt::init_s16vector(8, pilot_sets_arr[2]));
      pmt::vector_set(pilot_sets,  3, pmt::init_s16vector(8, pilot_sets_arr[3]));
      pmt::vector_set(pilot_sets,  4, pmt::init_s16vector(8, pilot_sets_arr[4]));
      pmt::vector_set(pilot_sets,  5, pmt::init_s16vector(8, pilot_sets_arr[5]));
      pmt::vector_set(pilot_sets,  6, pmt::init_s16vector(8, pilot_sets_arr[6]));
      pmt::vector_set(pilot_sets,  7, pmt::init_s16vector(8, pilot_sets_arr[7]));
      pmt::vector_set(pilot_sets,  8, pmt::init_s16vector(8, pilot_sets_arr[8]));
      pmt::vector_set(pilot_sets,  9, pmt::init_s16vector(8, pilot_sets_arr[9]));
      pmt::vector_set(pilot_sets, 10, pmt::init_s16vector(8, pilot_sets_arr[10]));
      pmt::vector_set(pilot_sets, 11, pmt::init_s16vector(8, pilot_sets_arr[11]));
      pmt::vector_set(pilot_sets, 12, pmt::init_s16vector(8, pilot_sets_arr[12]));

      //pmt::pmt_t mapping = pmt::make_dict();
      mapping = pmt::dict_add(mapping, pmt::string_to_symbol("range"), range );
      mapping = pmt::dict_add(mapping, pmt::string_to_symbol("unused_sets"), unused_sets );
      mapping = pmt::dict_add(mapping, pmt::string_to_symbol("pilot_sets"), pilot_sets );

      break;
    }
    case 2:
    {
      // Option 2
      //Range
      int16_t range_arr[2] = {-26, 26};
      pmt::pmt_t range        = pmt::init_s16vector(2, range_arr);

      //Unused carriers
      int16_t unused_set_arr[1] = {0};

      pmt::pmt_t unused_sets = pmt::make_vector(1, pmt::PMT_NIL);
      pmt::vector_set(unused_sets, 0, pmt::init_s16vector(1, unused_set_arr));

      //Pilot carriers
      int16_t pilot_sets_arr[7][4] = {
        {-14,  -2, 10, 22},
        {-22, -10,  2, 14},
        {-18,  -6,  6, 18},
        {-26, -14, -2, 26},
        {-22, -10, 10, 22},
        {-18,  -6,  2, 14},
        {-26,   6, 18, 26}
      };

      pmt::pmt_t pilot_sets = pmt::make_vector(7, pmt::PMT_NIL);
      pmt::vector_set(pilot_sets,  0, pmt::init_s16vector(4, pilot_sets_arr[0]));
      pmt::vector_set(pilot_sets,  1, pmt::init_s16vector(4, pilot_sets_arr[1]));
      pmt::vector_set(pilot_sets,  2, pmt::init_s16vector(4, pilot_sets_arr[2]));
      pmt::vector_set(pilot_sets,  3, pmt::init_s16vector(4, pilot_sets_arr[3]));
      pmt::vector_set(pilot_sets,  4, pmt::init_s16vector(4, pilot_sets_arr[4]));
      pmt::vector_set(pilot_sets,  5, pmt::init_s16vector(4, pilot_sets_arr[5]));
      pmt::vector_set(pilot_sets,  6, pmt::init_s16vector(4, pilot_sets_arr[6]));

      //pmt::pmt_t mapping = pmt::make_dict();
      mapping = pmt::dict_add(mapping, pmt::string_to_symbol("range"), range );
      mapping = pmt::dict_add(mapping, pmt::string_to_symbol("unused_sets"), unused_sets );
      mapping = pmt::dict_add(mapping, pmt::string_to_symbol("pilot_sets"), pilot_sets );
      
      break;
    }
    case 3:
    {
      // Option 3
      //Range
      int16_t range_arr[2] = {-13, 13};
      pmt::pmt_t range        = pmt::init_s16vector(2, range_arr);

      //Unused carriers
      int16_t unused_set_arr[1] = {0};

      pmt::pmt_t unused_sets = pmt::make_vector(1, pmt::PMT_NIL);
      pmt::vector_set(unused_sets, 0, pmt::init_s16vector(1, unused_set_arr));

      //Pilot carriers
      int16_t pilot_sets_arr[7][2] = {
        { -7,  7},
        {-11,  3},
        { -3, 11},
        { -9,  5},
        { -5,  9},
        {-13,  1},
        { -1, 13}
      };
      
      pmt::pmt_t pilot_sets = pmt::make_vector(7, pmt::PMT_NIL);
      pmt::vector_set(pilot_sets,  0, pmt::init_s16vector(2, pilot_sets_arr[0]));
      pmt::vector_set(pilot_sets,  1, pmt::init_s16vector(2, pilot_sets_arr[1]));
      pmt::vector_set(pilot_sets,  2, pmt::init_s16vector(2, pilot_sets_arr[2]));
      pmt::vector_set(pilot_sets,  3, pmt::init_s16vector(2, pilot_sets_arr[3]));
      pmt::vector_set(pilot_sets,  4, pmt::init_s16vector(2, pilot_sets_arr[4]));
      pmt::vector_set(pilot_sets,  5, pmt::init_s16vector(2, pilot_sets_arr[5]));
      pmt::vector_set(pilot_sets,  6, pmt::init_s16vector(2, pilot_sets_arr[6]));

      //pmt::pmt_t mapping = pmt::make_dict();
      mapping = pmt::dict_add(mapping, pmt::string_to_symbol("range"), range );
      mapping = pmt::dict_add(mapping, pmt::string_to_symbol("unused_sets"), unused_sets );
      mapping = pmt::dict_add(mapping, pmt::string_to_symbol("pilot_sets"), pilot_sets );
      
      break;
    }
    case 4:
    default:
    {
      // Option 4
      //Range
      int16_t range_arr[2] = {-7, 7};
      pmt::pmt_t range        = pmt::init_s16vector(2, range_arr);

      //Unused carriers
      int16_t unused_set_arr[1] = {0};

      pmt::pmt_t unused_sets = pmt::make_vector(1, pmt::PMT_NIL);
      pmt::vector_set(unused_sets, 0, pmt::init_s16vector(1, unused_set_arr));

      //Pilot carriers
      int16_t pilot_sets_arr[4][2] = {
        {-3, 5},
        {-7, 1},
        {-5, 3},
        {-1, 7}
      };
      
      pmt::pmt_t pilot_sets = pmt::make_vector(4, pmt::PMT_NIL);
      pmt::vector_set(pilot_sets,  0, pmt::init_s16vector(2, pilot_sets_arr[0]));
      pmt::vector_set(pilot_sets,  1, pmt::init_s16vector(2, pilot_sets_arr[1]));
      pmt::vector_set(pilot_sets,  2, pmt::init_s16vector(2, pilot_sets_arr[2]));
      pmt::vector_set(pilot_sets,  3, pmt::init_s16vector(2, pilot_sets_arr[3]));

      //pmt::pmt_t mapping = pmt::make_dict();
      mapping = pmt::dict_add(mapping, pmt::string_to_symbol("range"), range );
      mapping = pmt::dict_add(mapping, pmt::string_to_symbol("unused_sets"), unused_sets );
      mapping = pmt::dict_add(mapping, pmt::string_to_symbol("pilot_sets"), pilot_sets );
      
      break;
    }
  }

#ifdef DEBUGMODE
  pmt::pmt_t key_list = pmt::dict_keys( mapping );
  pdebug("Symbol tagger: func mapping keys no: %lu\n", pmt::length(key_list));
  for (size_t i = 0; i < pmt::length(key_list); i++ )
    pdebug("Symbol tagger: func mapping keys %lu.: %s\n", i, pmt::symbol_to_string( pmt::nth( i, key_list ) ).c_str());
#endif
    
  return mapping;
}


///////////////////////////////////////////////////////////////////////////////////
//
// Work method.
//
///////////////////////////////////////////////////////////////////////////////////
int
ieee802_15_4g_param_tagger_cc_impl::work(int                      noutput_items,
                                        gr_vector_const_void_star &input_items,
                                        gr_vector_void_star       &output_items)
{
  const gr_complex*   in  = (const gr_complex *) input_items[0];
  gr_complex*         out = (gr_complex *) output_items[0];

  memcpy ( (void *) out, (void *) in, noutput_items * sizeof(gr_complex) );
  
  uint64_t offset = 0;
          
  if ( d_option != d_option_prev && noutput_items >= d_symbol_len - d_residue )
  {
    pdebug("Symbol tagger: option change detected from %d to %d\n", d_option_prev, d_option);

    //Offset
    offset = d_symbol_len - d_residue;

    //Mapping symbol data and pilots
    pmt::pmt_t mapping = Generate_Mapping_Symbol();

#ifdef DEBUGMODE
    pmt::pmt_t key_list = pmt::dict_keys( mapping );
    pdebug("Symbol tagger: mapping keys no: %lu\n", pmt::length(key_list));
    for (size_t i = 0; i < pmt::length(key_list); i++ )
      pdebug("Symbol tagger: mapping keys %lu.: %s\n", i, pmt::symbol_to_string( pmt::nth( i, key_list ) ).c_str());
#endif

    //Add stream tag
    add_item_tag( 0, 
                nitems_written(0) + offset, 
                pmt::string_to_symbol("symbol_mapping"), 
                mapping,
                pmt::mp(alias()) );
/*    
    //Add optional pilot symbol reset tag if necessary
    add_item_tag( 0, 
                offset, 
      pmt::string_to_symbol("reset_pilots"), 
      pmt::PMT_T,
      pmt::mp(alias()) );
*/
    switch ( d_option )
    {
      case 1:
      {
        d_symbol_len = 2 * 52 + 1;
        break;
      }
      case 2:
      {
        d_symbol_len = 2 * 26 + 1;
        break;
      }
      case 3:
      {
        d_symbol_len = 2 * 13 + 1;
        break;
      }
      case 4:
      {
        d_symbol_len = 2 * 7 + 1;
        break;
      }
    }
    
    d_option_prev = d_option;
  }
  
  d_residue = (d_residue + noutput_items - offset) % d_symbol_len;
  
  // Tell runtime system how many output items we produced.
  return noutput_items;
}


} /* namespace Ben */
} /* namespace gr */
