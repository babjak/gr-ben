#ifndef INCLUDED_BEN_ADDITIVE_SCRAMBLER_H
#define INCLUDED_BEN_ADDITIVE_SCRAMBLER_H

#include <stdexcept>
#include <stdint.h>
#include <iostream>
#include <bitset>

/*!
 * \brief Fibonacci Linear Feedback Shift Register using specified
 * polynomial mask
 * \ingroup misc
 *
 * \details
 * Generates a maximal length pseudo-random sequence of length
 * 2^degree-1
 *
 * Constructor: digital::lfsr(int mask, int seed, int reg_len);
 *
 * \param mask - polynomial coefficients representing the
 *             locations of feedback taps from a shift register
 *             which are xor'ed together to form the new high
 *             order bit.
 *
 *             Some common masks might be:
 *              x^4 + x^3 + x^0 = 0x19
 *              x^5 + x^3 + x^0 = 0x29
 *              x^6 + x^5 + x^0 = 0x61
 *
 * \param seed - the initialization vector placed into the
 *             register durring initialization. Low order bit
 *             corresponds to x^0 coefficient -- the first to be
 *             shifted as output.
 *
 * \param reg_len - specifies the length of the feedback shift
 *             register to be used. Durring each iteration, the
 *             register is rightshifted one and the new bit is
 *             placed in bit reg_len. reg_len should generally be
 *             at least order(mask) + 1
 *
 *
 * see http://en.wikipedia.org/wiki/Linear_feedback_shift_register
 * for more explanation.
 *
 *  next_bit() - Standard LFSR operation
 *
 *      Perform one cycle of the LFSR.  The output bit is taken from
 *      the shift register LSB.  The shift register MSB is assigned from
 *      the modulo 2 sum of the masked shift register.
 *
 *  next_bit_scramble(unsigned char input) - Scramble an input stream
 *
 *      Perform one cycle of the LFSR.  The output bit is taken from
 *      the shift register LSB.  The shift register MSB is assigned from
 *      the modulo 2 sum of the masked shift register and the input LSB.
 *
 *  next_bit_descramble(unsigned char input) - Descramble an input stream
 *
 *      Perform one cycle of the LFSR.  The output bit is taken from
 *      the modulo 2 sum of the masked shift register and the input LSB.
 *      The shift register MSB is assigned from the LSB of the input.
 *
 */
    
class additive_scrambler
{
  private:
    uint32_t d_shift_register;
    uint32_t d_seed;
    uint32_t d_shift_register_length;

  public:
    additive_scrambler(uint32_t seed)
    : 
      d_shift_register(seed),
      d_seed(seed),
      d_shift_register_length(9)
    {
      pre_shift(1);
    }

    unsigned char next_bit()
    {
      unsigned char output = (d_shift_register & 0x100) >> 8;
    	unsigned char newbit = ((d_shift_register & 1) ^ ((d_shift_register >> 5) & 1));
    	d_shift_register = ((d_shift_register >> 1) | (newbit << 8));
    	return output;
    }

    unsigned char next_bit_scramble(unsigned char input)
    {
      return next_bit() ^ input;
    }
    
    unsigned char bitrev(unsigned char x)
    {
      unsigned char y = 0;
      int i;
      for(i=0;i<8;i++) 
        if(x & (1<<i)) 
          y|= 1<< (8-1-i);
      return y;
    }
        
    /*!
     * descramble a complete byte *LSB* first
     */
    unsigned char next_byte_descramble(unsigned char input_byte)
    {
      unsigned char res = 0;
      for(int b = 0; b < 8; ++b, input_byte <<= 1)  
        res |= (next_bit() ^ ((input_byte & 0x80) >> 7)) << b;
      return res;
    }
       
    /*!
     * Reset shift register to initial seed value
     */
    void reset() 
    { 
      d_shift_register = d_seed; 
      pre_shift(1);
    }

    /*!
     * Rotate the register through x number of bits
     * where we are just throwing away the results to get queued up correctly
     */
    void pre_shift(int num)
    {
      for(int i=0; i<num; i++)
    		next_bit();
    }

}; // class additive_scrambler

#endif /* INCLUDED_BEN_ADDITIVE_SCRAMBLER_H */
