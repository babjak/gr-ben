/* -*- c++ -*- */
/* 
 * Copyright 2014 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>

//#define DEBUGMODE 1
#ifdef DEBUGMODE
#define pdebug(...) do{if(DEBUGMODE)printf(__VA_ARGS__);}while(0)
#else
#define pdebug(...) do{}while(0)
#endif

#include <gnuradio/io_signature.h>
#include "zero_padding_in_fr_cc_impl.h"
#include <math.h> 

namespace gr {
namespace Ben {

///////////////////////////////////////////////////////////////////////////////////
//
//  Make.
//
///////////////////////////////////////////////////////////////////////////////////
zero_padding_in_fr_cc::sptr
zero_padding_in_fr_cc::make(int out_symbol_len)
{
  return gnuradio::get_initial_sptr(new zero_padding_in_fr_cc_impl(out_symbol_len));
}


///////////////////////////////////////////////////////////////////////////////////
//
//  The private constructor
//
///////////////////////////////////////////////////////////////////////////////////
zero_padding_in_fr_cc_impl::zero_padding_in_fr_cc_impl(int out_symbol_len)
: 
    gr::block("zero_padding_in_fr_cc",
            gr::io_signature::make(1, 1, sizeof(gr_complex)),
            gr::io_signature::make(1, 1, sizeof(gr_complex))),
    d_out_symbol_len(out_symbol_len),
    d_in_symbol_len(out_symbol_len),
    d_start_idx( (d_out_symbol_len - d_in_symbol_len)/2 )
{
    set_tag_propagation_policy( TPP_DONT );

    /////////////////////////
    // GNU radio HACK
    set_output_multiple(d_out_symbol_len);
}


///////////////////////////////////////////////////////////////////////////////////
//
// Our virtual destructor.
//
///////////////////////////////////////////////////////////////////////////////////
zero_padding_in_fr_cc_impl::~zero_padding_in_fr_cc_impl()
{
}


///////////////////////////////////////////////////////////////////////////////////
//
// Forecasting output size.
//
///////////////////////////////////////////////////////////////////////////////////
void
zero_padding_in_fr_cc_impl::forecast (int            noutput_items, 
                                    gr_vector_int   &ninput_items_required)
{
    ninput_items_required[0] = ((int) ceil( (double) noutput_items / (double) d_out_symbol_len )) * d_in_symbol_len;
}


///////////////////////////////////////////////////////////////////////////////////
//
// Work method.
//
///////////////////////////////////////////////////////////////////////////////////
int
zero_padding_in_fr_cc_impl::general_work (int                        noutput_items,
                                        gr_vector_int               &ninput_items,
                                        gr_vector_const_void_star   &input_items,
                                        gr_vector_void_star         &output_items)
{
  const gr_complex*   in          = (const gr_complex*) input_items[0];
  gr_complex*         out         = (gr_complex*) output_items[0];
  int                 nin         = 0; //no. of processed input samples
  int                 nout        = 0; //no. of generated output samples

  uint64_t            abs_N       = nitems_read(0);
  uint64_t            end_abs_N   = abs_N + (uint64_t) ninput_items[0];

  std::vector<tag_t>           tags;
  get_tags_in_range( tags, 0, abs_N, end_abs_N );
  std::vector<tag_t>::iterator tags_itr;
  tags_itr = tags.begin();
    
  int in_stop_idx = nin;

  do
  {
    //Look for the end of input buffer or the next tag with different setup
    nin = in_stop_idx; 
    in_stop_idx = ninput_items[0];
    
    for ( ; tags_itr != tags.end(); tags_itr++ )
    {
      pdebug( "Symbol mapper: Found a stream tag: %s\n", pmt::symbol_to_string(tags_itr->key).c_str() );
      if ( pmt::symbol_to_string(tags_itr->key) != "symbol_mapping" || !pmt::is_dict(tags_itr->value) )
        continue;

      int tag_idx = tags_itr->offset - abs_N;
      pdebug( "Symbol mapper: nin: %d, tag_idx: %d\n", nin, tag_idx );
      if ( nin > tag_idx )
        continue;
            
      if ( nin < tag_idx )
      {
        in_stop_idx = tag_idx;
        pdebug( "Symbol mapper: in_stop_idx: %d\n", in_stop_idx );
        break;
      }
        
      //There was a new stream tag with a different setup
      printf("Zero padding: New stream tag with a different setup detected at sample %lu.\n", tags_itr->offset );
      pmt::pmt_t mapping = tags_itr->value;

      pmt::pmt_t range = pmt::dict_ref( mapping, pmt::string_to_symbol("range"), pmt::PMT_NIL );
      if ( range != pmt::PMT_NIL && pmt::is_s16vector(range) && pmt::length(range) == 2 )
      {
        int16_t t[2];

        t[0] = fmin( pmt::s16vector_elements( range )[0], pmt::s16vector_elements( range )[1] );
        t[1] = fmax( pmt::s16vector_elements( range )[0], pmt::s16vector_elements( range )[1] );

        d_in_symbol_len = t[1] - t[0] + 1;
        printf("Zero padding: Input symbol length: %d - %d + 1 = %d\n", t[1], t[0], d_in_symbol_len);
        d_start_idx = (d_out_symbol_len - d_in_symbol_len)/2;
      }
            
      add_item_tag( 0, 
                    nitems_written(0) + nout, 
                    tags_itr->key, 
                    tags_itr->value,
                    pmt::mp(alias()) );
    }

    //Process input samples until the end of input buffer or the next tag with different setup
    while ( nin + d_in_symbol_len <= in_stop_idx && nout + d_out_symbol_len <= noutput_items )
    {
      pdebug("Zero padding: Reset output\n");
      memset( out + nout, 0, d_out_symbol_len * sizeof(gr_complex) );
      memcpy( out + nout + d_start_idx, in + nin, d_in_symbol_len * sizeof(gr_complex) );
            
      nout += d_out_symbol_len;
      nin += d_in_symbol_len;
      pdebug("Zero padding: nin: %d, nout: %d\n", nin, nout);
    }

  } while ( nin + d_in_symbol_len > in_stop_idx && tags_itr != tags.end() && nout + d_out_symbol_len <= noutput_items );

  pdebug("Zero padding: nin: %d\n", nin);
  pdebug("Zero padding: nout: %d\n", nout);
  // Tell runtime system how many input items we consumed on
  // each input stream.
  consume_each(nin);

  // Tell runtime system how many output items we produced.
  return nout;
}


} /* namespace Ben */
} /* namespace gr */
