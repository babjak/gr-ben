/* -*- c++ -*- */
/* 
 * Copyright 2014 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_BEN_SYMBOL_CARRIER_MAPPER_CC_IMPL_H
#define INCLUDED_BEN_SYMBOL_CARRIER_MAPPER_CC_IMPL_H

#include <Ben/symbol_carrier_mapper_cc.h>
#include <additive_scrambler.h>

namespace gr {
namespace Ben {

class symbol_carrier_mapper_cc_impl 
: 
  public symbol_carrier_mapper_cc
{
  private:
        int16_t                             d_range[2];
        std::vector< std::vector<int16_t> > d_unused_sets;
        uint32_t                            d_unused_set_cnt;
        std::vector< std::vector<int16_t> > d_pilot_sets;
        uint32_t                            d_pilot_set_cnt;
            
        unsigned short                      d_out_symbol_len;

        int                                 d_hack_nout_size;
            
        additive_scrambler                  *d_pilot_gen;
            
            
        unsigned char Generate_OFDM_Symbols (const gr_complex*  &in,
                                            const int           ninput_items,
                                            gr_complex*         &out,
                                            const int           noutput_items,
                                            int                 &nin,
                                            int                 &nout);

        void Process_Stream_Tag(pmt::pmt_t &mapping);

  public:
    symbol_carrier_mapper_cc_impl();
    ~symbol_carrier_mapper_cc_impl();

    // Where all the action really happens
    void forecast (int noutput_items, gr_vector_int &ninput_items_required);

    int general_work(int                        noutput_items,
                    gr_vector_int               &ninput_items,
                    gr_vector_const_void_star   &input_items,
                    gr_vector_void_star         &output_items);
};

} // namespace Ben
} // namespace gr

#endif /* INCLUDED_BEN_SYMBOL_CARRIER_MAPPER_CC_IMPL_H */

