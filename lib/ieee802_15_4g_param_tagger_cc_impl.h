/* -*- c++ -*- */
/* 
 * Copyright 2014 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_BEN_IEEE802_15_4G_PARAM_TAGGER_CC_IMPL_H
#define INCLUDED_BEN_IEEE802_15_4G_PARAM_TAGGER_CC_IMPL_H

#include <Ben/ieee802_15_4g_param_tagger_cc.h>

namespace gr {
namespace Ben {

class ieee802_15_4g_param_tagger_cc_impl 
: 
  public ieee802_15_4g_param_tagger_cc
{
  private:
    int     d_option;
    int     d_option_prev;
    size_t  d_residue;
    size_t  d_symbol_len;

    pmt::pmt_t      Generate_Mapping_Symbol();

  public:
    ieee802_15_4g_param_tagger_cc_impl(int option);
    ~ieee802_15_4g_param_tagger_cc_impl();

    // Where all the action really happens
    int work(int                        noutput_items,
            gr_vector_const_void_star   &input_items,
            gr_vector_void_star         &output_items);
        
    int     option() const { return d_option; }
    void    set_option(int option) { d_option = option; }
};

} // namespace Ben
} // namespace gr

#endif /* INCLUDED_BEN_IEEE802_15_4G_PARAM_TAGGER_CC_IMPL_H */

